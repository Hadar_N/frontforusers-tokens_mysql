import logo from './logo.svg';
import './App.css';
import styled from 'styled-components';

import ConnAndData from './ConnectAndData'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <HeaderInside><div className="headerTitle">Auth Demo </div><img src={logo} className="App-logo" alt="logo" /></HeaderInside>
        <p />
        <ConnAndData />
      </header>
    </div>
  );
}

export default App;

const HeaderInside = styled.div`
  display: flex;
  align-items: center;
  height: 5rem;
  font-size: 2rem;
  font-weight: 600;
  white-space: nowrap;
`;