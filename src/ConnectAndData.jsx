import styled from 'styled-components';
import {useCountdown} from "./useCountdown";

const userInfo = 
  {
    email      : "3rd@last.com",
    password   : "pass"
  }

const ConnAndData = () => {
  const [countTime,startCouter] = useCountdown();

    const handleLogin = (e) => {
        console.log(localStorage);
        startCouter(10)
    }
    const handleGetData = (e) => {
      console.log("pressed data btn")
    }

  return (
    <Container>

    {countTime}

    <div>Authenticated: <ColoredText>false</ColoredText></div>
    <div><ColoredText>user:</ColoredText></div>
    <CodeText>null</CodeText>


    <ButtonsContainer>
      <Button onClick={handleLogin}>login</Button>
      <Button onClick={handleGetData}>get secret data</Button>
    </ButtonsContainer>

    no content yet...

    </Container>
  );
}

export default ConnAndData;

const Container = styled.div`
@import url('https://fonts.googleapis.com/css2?family=Cutive+Mono&display=swap');

    display: grid;
    justify-items: center;
    gap: 1rem;
    font-size: 1.2rem;
`;

const Button = styled.button`
    font: inherit;
    background: #61dafb;
    border: none;
    border-radius: 2px;
    line-height: 1.5;
`; 

const ButtonsContainer = styled.div`
  display :flex;
  gap: .5rem;
`;



const ColoredText = styled.span`
    font: inherit;
    color: #61dafb;
`;

const CodeText = styled.span`
    font: inherit;
    font-family: "Cutive Mono";
`;  
