import { useState, useEffect, useCallback } from "react";

let intervalID;

export const useCountdown = () => {
  const [currNum, setCurrNum] = useState(0);

  const changeStateOnInterval = useCallback(() => {
    setCurrNum((c) => c - 1);
  }, [setCurrNum]);

  useEffect(() => {
    if (currNum === 0) clearInterval(intervalID);
  }, [currNum, changeStateOnInterval]);

  const startCount = (startFrom) => {
    setCurrNum((c) => c + startFrom);
    intervalID = setInterval(changeStateOnInterval, 1000);
  }

  return [currNum,startCount];
};
